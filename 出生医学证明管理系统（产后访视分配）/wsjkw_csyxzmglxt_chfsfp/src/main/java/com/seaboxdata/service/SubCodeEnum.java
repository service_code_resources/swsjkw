package com.seaboxdata.service;


/**
 * @Author dengshengyu
 * @Description //TODO
 * @Date 10:47 上午 2020/5/22
 * @Param
 * @return
 **/
public enum SubCodeEnum {
	SUB_CODE_ENUM0(0, "正常返回,且有数据数据"),SUB_CODE_ENUM1(1, "请求异常"),SUB_CODE_ENUM2(2, "正常返回，但没有数据");

	private final Integer code;
	private final String describe;


	//声明一个构造方法
	SubCodeEnum(Integer code, String value){
		this.code=code;
		this.describe=value;
	}

	public Integer getCode() {
		return code;
	}

	public String getDescribe() {
		return describe;
	}

}
