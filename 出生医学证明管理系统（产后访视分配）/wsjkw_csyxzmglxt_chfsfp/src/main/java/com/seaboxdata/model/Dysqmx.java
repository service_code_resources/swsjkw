package com.seaboxdata.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @Description TODO(返回数据)
 * @Author xhq
 * @Date 2020/4/8 15:03
 **/
@Data
public class Dysqmx {
    @JsonProperty
    private String cSRQ;
    @JsonProperty
    private String fSDH;
    @JsonProperty
    private String id;
    @JsonProperty
    private String jSJGMC;
    @JsonProperty
    private String mQCHXYDZ;
    @JsonProperty
    private String mQCHXYQX;
    @JsonProperty
    private String mQCZQX;
    @JsonProperty
    private String mQXM;
    @JsonProperty
    private String qFSJ;

}
