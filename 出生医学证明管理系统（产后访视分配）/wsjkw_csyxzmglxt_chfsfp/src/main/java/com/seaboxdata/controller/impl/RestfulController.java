package com.seaboxdata.controller.impl;

import com.seaboxdata.controller.IRestfulController;
import com.seaboxdata.service.DataService;
import com.seaboxdata.service.SubResultInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class RestfulController implements IRestfulController {
    @Autowired
    private DataService dataService;


    @Override
    public ResponseEntity getDataJson(Integer pageNo, Integer pageSize, String search, String token,HttpServletRequest request) {
//        System.out.println(token);
//        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").disableHtmlEscaping().create();
        SubResultInfo data = dataService.getData(pageNo, pageSize, search);
//        java.util.Map dataJsonResponse = DataResponse.getDataJsonResponse(gson.toJson(data));
        return new ResponseEntity(data, HttpStatus.OK);
    }

}
