package com.seaboxdata.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;


@RequestMapping("/services/${serviceCode}")
public interface IRestfulController {
    @GetMapping(value = "/getDataJson", produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity getDataJson(@RequestParam(value = "pageNo", required = false) Integer pageNo,
                               @RequestParam(value = "pageSize", required = false) Integer pageSize,
                               @RequestParam(value = "search", required = false) String search,
                               @RequestParam(value = "token", required = false) String token,
                               HttpServletRequest request);
 }
