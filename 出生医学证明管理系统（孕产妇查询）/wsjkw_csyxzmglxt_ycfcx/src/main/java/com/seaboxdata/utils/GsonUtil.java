package com.seaboxdata.utils;/**
 * Created by dengshengyu on 2020/6/18
 */

import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

/**
 * @ClassName Gson
 * @Author dengshengyu
 * @Description //TODO 
 *
 * @Date 2020/6/18 12:07 下午
 **/

public class GsonUtil {

    public com.google.gson.Gson createGson(){
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Integer.class,new IntegerDefaultAdapter());
        builder.registerTypeAdapter(String.class,new StringDefaultAdapter());
        return builder.create();
    }

    public class IntegerDefaultAdapter extends TypeAdapter<Integer>{

        @Override
        public void write(JsonWriter jsonWriter, Integer integer) throws IOException {
            jsonWriter.value(String.valueOf(integer));
        }

        @Override
        public Integer read(JsonReader jsonReader) throws IOException {
            try {
                return Integer.valueOf(jsonReader.nextString());
            }catch (NumberFormatException e){
                e.printStackTrace();
                return -1;
            }
        }
    }

    public class StringDefaultAdapter extends TypeAdapter<String> {

        @Override
        public void write(JsonWriter jsonWriter, String s) throws IOException {
            jsonWriter.value(s);
        }

        @Override
        public String read(JsonReader jsonReader) throws IOException {
            if (jsonReader.peek() == JsonToken.NULL){
                jsonReader.nextNull();
                return "";
            }else{
                return jsonReader.nextString();
            }
        }
    }
}
