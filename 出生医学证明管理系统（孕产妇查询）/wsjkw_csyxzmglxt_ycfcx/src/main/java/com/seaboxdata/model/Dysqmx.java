package com.seaboxdata.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @Description TODO(返回数据)
 * @Author xhq
 * @Date 2020/4/8 15:03
 **/
@Data
public class Dysqmx {

    @JsonProperty
    private String mQXM;
    @JsonProperty
    private String mQNL;
    @JsonProperty
    private String mCGJ;
    @JsonProperty
    private String mQMZ;
    @JsonProperty
    private String mQZJHM;
    @JsonProperty
    private String mQCZDZ;
    @JsonProperty
    private String mQCHXYDZ;
    @JsonProperty
    private String cZSJ;

    @JsonProperty
    private String gDBZ;

    @JsonProperty
    private String id;

    @JsonProperty
    private String mQHJDZ;
}
