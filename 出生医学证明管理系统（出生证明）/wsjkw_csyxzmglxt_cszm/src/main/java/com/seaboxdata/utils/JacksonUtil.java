package com.seaboxdata.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 *  解析来自不同服务返回的json字符串
 *
 * @author lvchong
 */
@Slf4j

public class JacksonUtil {
    public static String getJson(Object result){
        ObjectMapper mapper = new ObjectMapper();
        String str = null;
        try {
            str = mapper.writeValueAsString(result);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return str;
    }

}
