package com.seaboxdata.model;

import lombok.Data;

/**
 * @Description TODO(返回数据)
 * @Author xhq
 * @Date 2020/4/8 15:03
 **/
@Data
public class Dysqmx {
    private String xsrxm;
    private String cszbh;
    private String xsrxb;
    private String csrq;
    private String csddz;
    private String cszbm;
    private String mqxm;
    private String mqzjhm;
    private String mqhjdz;
    private String mqczdz;
    private String fqxm;
    private String fqzjhm;
    private String fqhjdz;
    private String fqczdz;
}
