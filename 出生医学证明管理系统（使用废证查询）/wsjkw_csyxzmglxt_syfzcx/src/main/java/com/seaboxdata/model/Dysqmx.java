package com.seaboxdata.model;

import lombok.Data;

/**
 * @Description TODO(返回数据)
 * @Author xhq
 * @Date 2020/4/8 15:03
 **/
@Data
public class Dysqmx {
    private String cszbh;
    private String czyxm;
    private String id;
    private String mqczdz;
    private String mqhjdz;
    private String mqzjhm;
    private String sqjgmc;

}
