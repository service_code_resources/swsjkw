package com.seaboxdata.service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.seaboxdata.model.Dysqmx;
import com.seaboxdata.utils.EncryptUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
@Component
public class DataService {

    @Value("${custom.url}")
    private String url;

    @Value("${custom.sKey}")
    private String sKey;


    public SubResultInfo getData(Integer pageNo, Integer pageSize, String search) {
        SubResultInfo subResultInfo = new SubResultInfo();
        pageNo = (null == pageNo || pageNo < 1) ? 1 : pageNo;
        pageSize = (null == pageSize || pageSize < 1) ? 10 : pageSize;
        pageSize = (pageSize > 100) ? 100 : pageSize;

        Gson gson = new Gson();
        List<Dysqmx> data;
        HttpResponse response;
        Integer total = null;
        int code = 0;
        try {
            response = this.obtainData(pageNo, pageSize, search);
            code = response.getStatusLine().getStatusCode();
            if (code != 200) {
                subResultInfo.setCode(SubCodeEnum.SUB_CODE_ENUM1.getCode());
                data = gson.fromJson("[]", new TypeToken<List<Dysqmx>>() {
                }.getType());
                subResultInfo.setMessage(SubCodeEnum.SUB_CODE_ENUM1.getDescribe());
            } else {
                String content = EntityUtils.toString(response.getEntity());
                System.out.println(content);
                //解密
                content = EncryptUtils.Decrypt(content, sKey);
                System.out.println(content);
                JSONObject jsonObj = new JSONObject(content);
                JSONObject json = jsonObj.getJSONObject("data");
                total = json.getInt("total");
                String result = json.get("rows").toString();
                if (!result.startsWith("[")) {
                        result = "[" + result + "]";
                }

                data = gson.fromJson(result, new TypeToken<List<Dysqmx>>() {
                }.getType());
                subResultInfo.setCode(SubCodeEnum.SUB_CODE_ENUM0.getCode());
                subResultInfo.setMessage(SubCodeEnum.SUB_CODE_ENUM0.getDescribe());
            }
            subResultInfo.setTotal(total);
            subResultInfo.setResult(data);
        } catch (Exception e) {
            if (code == 200) {
                subResultInfo.setCode(SubCodeEnum.SUB_CODE_ENUM2.getCode());
                data = gson.fromJson("[]", new TypeToken<List<Dysqmx>>() {
                }.getType());
                subResultInfo.setTotal(total);
                subResultInfo.setCode(SubCodeEnum.SUB_CODE_ENUM2.getCode());
                subResultInfo.setResult(data);
                subResultInfo.setMessage(SubCodeEnum.SUB_CODE_ENUM2.getDescribe());
            }else{
                subResultInfo.setCode(SubCodeEnum.SUB_CODE_ENUM1.getCode());
                data = gson.fromJson("[]", new TypeToken<List<Dysqmx>>() {
                }.getType());
                subResultInfo.setTotal(total);
                subResultInfo.setResult(data);
                subResultInfo.setMessage(SubCodeEnum.SUB_CODE_ENUM1.getDescribe());
            }
        }
        subResultInfo.setIsPageable("1");
        return subResultInfo;
    }

    /**
     * @Author dengshengyu
     * @Description //TODO 
     * @Date 1:07 下午 2020/6/6
     * @Param [page, pagesize, search]
     * @return org.apache.http.HttpResponse
     **/
    public HttpResponse obtainData(Integer page, Integer pagesize, String search) throws Exception {

        System.out.println("请求数据");
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);
        //post参数
        List<NameValuePair> parameter = new ArrayList<NameValuePair>();
        parameter.add(new BasicNameValuePair("pageNum", page.toString()));
        parameter.add(new BasicNameValuePair("pageSize", pagesize.toString()));
//	        parameter.add(new BasicNameValuePair("queryParam",parseSearch(serch)));
        httpPost.setEntity(new UrlEncodedFormEntity(parameter, HTTP.UTF_8));
        HttpResponse response = httpclient.execute(httpPost);
        return response;
    }

    private String jointQueryConditon(Integer page, Integer pagesize, String search) {
        String param = "{\"countPerPage\":\"" + pagesize + "\",\"currentPage\":\"" + page + "\"";

        String parseSearch = parseSearch(search);
        if (!StringUtils.isEmpty(parseSearch)) {
            param += "," + parseSearch;
        }
        param += "}";
        return param;
    }
    public static String parseSearch(String search) {
        if (StringUtils.isBlank(search)) {
            return "";
        }
        search = search.replaceAll(".eq|.neq|.gt|.gte|.lt|.lte|.like|.in|.isNull|.isNotNull|and.", "");

        JSONArray jsonArr = new JSONArray(search);
        JSONObject jsonObj = null;

        String key = "";
        String value = "";
        String result = "";

        for (int i = 0; i < jsonArr.length(); i++) {
            jsonObj = jsonArr.getJSONObject(i);
            key = jsonObj.keys().next();
            value = jsonObj.getString(key);
            result += "\"" + key + "\":\"" + value + "\",";
        }
        result = result.substring(0, result.length() - 1);
        return result;
    }
    /**
     * 提取查询条件（该方法可复用）
     *
     * @param search
     * @return <条件名称全小写，条件值>
     * @author dengshengyu
     * @date 2020年5月7日上午11:57:02
     */
    private Map<String, String> parseSearchs(String search) {
        Map<String, String> map = new HashMap<>();
        if (search == null || "".equals(search.trim())) {
            return map;
        }
        JSONArray jsonArr = new JSONArray(search);
        for (int i = 0; i < jsonArr.length(); i++) {
            JSONObject jsonObj = jsonArr.getJSONObject(i);
            String key = jsonObj.keys().next();
            String value = jsonObj.getString(key);
            key = key.toLowerCase();
            key = key.replace(".eq", "");
            key = key.replace(".like", "");
            key = key.replace(".gt", "");
            key = key.replace(".lt", "");
            key = key.replace(".or", "");
            key = key.replace(".and", "");
            key = key.replace(".gte", "");
            key = key.replace(".lte", "");
            key = key.replace(".in", "");
            key = key.replace(".isNotNull", "");
            key = key.replace(".isNull", "");
            map.put(key.toLowerCase(), value);
        }
        return map;
    }


}

