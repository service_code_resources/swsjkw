package com.seaboxdata.utils;

import org.apache.commons.lang3.StringUtils;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


// 加密
public class EncryptUtils {

    //@Value("${crypt.ivParameterSpec}")
    private static String ivParameterSpec = "0102030405060708";
    //@Value("${crypt.keySpec}")
    private static String keySpec = "AES";

    public static String Encrypt(String sSrc, String sKey) throws Exception {
        if (sKey == null) {
           // System.out.print("Key为空null");
            return null;
        }
        // 判断Key是否为16位
        if (sKey.length() != 16) {
          //  System.out.print("Key长度不是16位");
            return null;
        }
        byte[] raw = sKey.getBytes();
        SecretKeySpec skeySpec = new SecretKeySpec(raw, keySpec);
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");//"算法/模式/补码方式"
        IvParameterSpec iv = new IvParameterSpec(ivParameterSpec.getBytes());//使用CBC模式，需要一个向量iv，可增加加密算法的强度
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
        byte[] encrypted = cipher.doFinal(sSrc.getBytes());


        return new BASE64Encoder().encode(encrypted);//此处使用BASE64做转码功能，同时能起到2次加密的作用。
    }


    // 解密
    public static String Decrypt(String sSrc, String sKey) throws Exception {
        try {
            // 判断Key是否正确
            if (sKey == null) {
                return null;
            }
            // 判断Key是否为16位
            if (sKey.length() != 16) {
                return null;
            }
            if(StringUtils.isBlank(sSrc)){
                return null;
            }
            byte[] raw = sKey.getBytes("ASCII");
            SecretKeySpec skeySpec = new SecretKeySpec(raw, keySpec);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            IvParameterSpec iv = new IvParameterSpec(ivParameterSpec
                    .getBytes());
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
            byte[] encrypted1 = new BASE64Decoder().decodeBuffer(sSrc);//先用base64解密
            try {
                byte[] original = cipher.doFinal(encrypted1);
                String originalString = new String(original);
                return originalString;
            } catch (Exception e) {
                return null;
            }
        } catch (Exception ex) { System.out.println(ex.toString());

            return null;
        }
    }


    public static void main(String[] args) throws Exception {
        /*
         * 加密用的Key 可以用26个字母和数字组成，最好不要用保留字符，虽然不会错，至于怎么裁决，个人看情况而定
         * 此处使用AES-128-CBC加密模式，key需要为16位     +  base64加密。
         */
        String cKey = "1234567891111111";
        StringBuffer markIds = new StringBuffer();
        markIds.append(1).append(",");
        String replace = markIds.substring(0, markIds.length() - 1).replace(",", "','");

        String time = "1490840793103";
        String sysName = "haha";
        String orgCode = "13456789";

//--自定义加密规则

        String keyString = sysName + time + orgCode;
        keyString = keyString.substring(keyString.length()-6, keyString.length()
        )+keyString.substring(0, keyString.length() - 6);
        /*keyString = "{\n" +
                "\t\"hdsd0011110\":\"橙子\",\n" +
                "\t\"exhdsd0202260\":\"01\",\n" +
                "\t\"exhdsd0202238\":\"102312\",\n" +
                "\t\"exhdsd0202239\":\"姐姐\",\n" +
                "\t\"exhdsd0203104\":\"11234\",\n" +
                "\t\"exhdsd0202258\":\"50010319850420551X\",\n" +
                "\t\"exhdsd0202261\":\"2018-07-20\"\n" +
                "}";*/

        keyString = "{\n" +
                "\t\"name\":\"橙子\",\n" +
                "\t\"sexcode\":\"2\",\n" +
                "\t\"birthday\":\"19090719\",\n" +
                "\t\"idcard\":\"511231958020512345\",\n" +
                "\t\"poplocalid\":\"11234\",\n" +
                "\t\"outorgancode\":\"01\",\n" +
                "\t\"outorganname\":\"重庆\"\n" +
                "}";

        // 需要加密的字串
        String cSrc = keyString;
        System.out.println(cSrc);
        // 加密
        long lStart = System.currentTimeMillis();
        System.out.println(System.currentTimeMillis());
        String enString = EncryptUtils.Encrypt(cSrc, cKey);
        System.out.println("加密后的字串是：" + enString);


        long lUseTime = System.currentTimeMillis() - lStart;
        System.out.println("加密耗时：" + lUseTime + "毫秒");
        // 解密
        lStart = System.currentTimeMillis();
        String DeString = EncryptUtils.Decrypt(enString, cKey);
        System.out.println("解密后的字串是：" + DeString);
        lUseTime = System.currentTimeMillis() - lStart;
        System.out.println("解密耗时：" + lUseTime + "毫秒");

        System.out.println(System.currentTimeMillis());
    }
}
