package com.seaboxdata.model;

import lombok.Data;

/**
 * @Description TODO(返回数据)
 * @Author xhq
 * @Date 2020/4/8 15:03
 **/
@Data
public class Dysqmx {
    private String cjjgmc;
    private String csrq;
    private String csyz;
    private String cszc;
    private String cztz;
    private String id;
    private String jkzt;
    private String jsrxm;
    private String mqxm;
    private String sjly;
    private String xsrxb;

    private String csddz;

}
