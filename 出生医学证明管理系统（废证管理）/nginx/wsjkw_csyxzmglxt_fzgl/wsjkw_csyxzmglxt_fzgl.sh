#!/bin/bash
#这里可替换为你自己的执行程序，其他代码无需更改

APP_NAME=wsjkw_csyxzmglxt_fzgl
APP_JAR=wsjkw_csyxzmglxt_fzgl-0.0.1-SNAPSHOT.jar

APP_HOME=$(cd `dirname $0`; pwd)
JAR_FILE=${APP_HOME}/${APP_JAR}
SPRING_CONFIG_FILE=${APP_HOME}/application.yml
START_LOG_PATH=${APP_HOME}/${APP_NAME}-start.log

JAVA_OPTS="-server -Xms1g -Xmx1g -XX:PermSize=128M -XX:MaxPermSize=512m "
JAVA_OPTS+="-XX:NewRatio=2 -XX:SurvivorRatio=8 -XX:+UseConcMarkSweepGC -XX:ParallelCMSThreads=8"

SPRING_OPTS="--spring.config.location="${SPRING_CONFIG_FILE}

#使用说明，用来提示输入参数
usage() {
    echo "Usage: sh 执行脚本.sh [start|stop|restart|status]"
    exit 1
}

#检查程序是否在运行
is_exist(){
  pid=`ps -ef|grep ${APP_JAR}|grep -v grep|awk '{print $2}' `
  #如果不存在返回1，存在返回0
  if [ -z "${pid}" ]; then
   return 1
  else
    return 0
  fi
}

#启动方法
start(){
  is_exist
  if [ $? -eq "0" ]; then
    echo "${APP_NAME} is already running. pid=${pid} ."
  else
    nohup java -jar ${JAVA_OPTS} ${JAR_FILE} ${SPRING_OPTS} > ${START_LOG_PATH} 2>&1 &
  fi
}

#停止方法
stop(){
  is_exist
  if [ $? -eq "0" ]; then
    kill -9 $pid
  else
    echo "${APP_NAME} is not running"
  fi
}

#输出运行状态
status(){
  is_exist
  if [ $? -eq "0" ]; then
    echo "${APP_NAME} is running. Pid is ${pid}"
  else
    echo "${APP_NAME} is NOT running."
  fi
}

#重启
restart(){
  stop
  start
}

#根据输入参数，选择执行对应方法，不输入则执行使用说明
case "$1" in
  "start")
    start
    ;;
  "stop")
    stop
    ;;
  "status")
    status
    ;;
  "restart")
    restart
    ;;
  *)
    usage
    ;;
esac

