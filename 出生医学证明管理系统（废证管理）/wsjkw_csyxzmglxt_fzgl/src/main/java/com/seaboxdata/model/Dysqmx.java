package com.seaboxdata.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @Description TODO(返回数据)
 * @Author xhq
 * @Date 2020/4/8 15:03
 **/
@Data
public class Dysqmx {
    @JsonProperty
    private String cszbh;
    @JsonProperty
    private String czyxm;
    @JsonProperty
    private String id;
    @JsonProperty
    private String mqczdz;
    @JsonProperty
    private String mqhjdz;
    @JsonProperty
    private String mqzjhm;
    @JsonProperty
    private String sqjgmc;



    /*@JsonProperty   老
    private String grzfrsj;
    @JsonProperty
    private String zsl;
    @JsonProperty
    private String zflx;
    @JsonProperty
    private String zfyy;
    @JsonProperty
    private String sbjgmc;
    @JsonProperty
    private String jbr;*/
}
