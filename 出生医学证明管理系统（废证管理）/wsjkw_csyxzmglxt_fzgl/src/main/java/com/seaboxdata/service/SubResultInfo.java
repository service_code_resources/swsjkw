package com.seaboxdata.service;/**
 * Created by dengshengyu on 2020/5/22
 */

import lombok.Data;

import java.util.List;

/**
 * @ClassName ResultInfos
 * @Author dengshengyu
 * @Description //TODO 
 *
 * @Date 2020/5/22 7:06 下午
 **/
@Data
public class SubResultInfo<T> {
    private int code;//0：成功；1：没有数据，原始返回为空；2：请求异常
    private String message;
    private List<T> result;
    private Integer total;
}
